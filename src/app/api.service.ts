import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { cd_family_details } from "./modules/cd_familydetails";
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { }

  API_KEY = 'bf80fb39094340e4a012c879a0bfa1b3';

  getNews() {
    return this.httpClient.get(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
  }

  AAA_login() {
    var param = {
      "username": "scott",
      "password": "tiger",
      "device_id": "123456",
      "unique_id": "123456"
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.httpClient.post(
      `https://aaa.allianz.lk/frontend/web/index.php?r=user/getarenamobile`,
      param,
      httpOptions
    );
  }

  save_customer_info(){
    
  }
  getAusysToken(){
    // var param = new HttpParams().set("grant_type=client_credentials");
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':'Basic ZXByb3AtY2xpZW50OmVwcm9wLXNlY3JldA=='
      })
    };
    //http://10.71.64.76:8081/oauth/token
    //http://DESKTOP-2VIIB4D:8081/oauth/token
    return this.httpClient.post(
      `http://localhost:8081/oauth/token`,
      "grant_type=client_credentials",
      httpOptions
    );
  }
  save_CD_expence_info(details:cd_family_details){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }

  // save customer...
  saveCustomer(customerObj) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'bearer 8862d1f2-87df-4e22-a70f-561f8da4bcf5'
      })
    }
    return this.httpClient.post<any[]>(
      `http://localhost:8081/api/saveFnaCustomerDetail?userToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhYWEuYWxsaWFuei5sayIsImF1ZCI6ImFhYS5hbGxpYW56LmxrIiwiaWF0IjoxNTgxNDkzNjg5LCJuYmYiOjE1ODE0OTM2OTksImV4cCI6MTU4MTQ5NzI5OX0.eh4dAuzTsdPT8TgDBXwnyCO5YwCZ6MJsf9eFxw6Ohks`,
      JSON.stringify(customerObj),
      httpOptions
    );
  }

  // save financial streangth details...
  saveFinancialStreangthDetails(financialStreangthDetails) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'bearer 8862d1f2-87df-4e22-a70f-561f8da4bcf5'
      })
    }
    return this.httpClient.post<any[]>(
      `http://localhost:8081/api/saveFinancialStrength`,
      JSON.stringify(financialStreangthDetails),
      httpOptions
    );
  }

  // save cash and income need details...
  saveCashAndIncomeDetails(cashAndIncomeDetails) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'bearer 8862d1f2-87df-4e22-a70f-561f8da4bcf5'
      })
    }
    return this.httpClient.post<any[]>(
      `http://localhost:8081/api/saveCashAndIncome`,
      JSON.stringify(cashAndIncomeDetails),
      httpOptions
    );
  }

  // save retirement and investment details...
  saveRetirementAndDetails(retirementDetails) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'bearer 8862d1f2-87df-4e22-a70f-561f8da4bcf5'
      })
    }
    return this.httpClient.post<any[]>(
      `http://localhost:8081/api/saveRetirementAndInvestment`,
      JSON.stringify(retirementDetails),
      httpOptions
    );
  }

  // save summary details...
  saveSummaryDetails(contacNo, PrioritizA) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'bearer 8862d1f2-87df-4e22-a70f-561f8da4bcf5'
      })
    }
    return this.httpClient.post<any[]>(
      `http://localhost:8081/api/saveSummaryAndPriority?contactNo=${contacNo}`,
      JSON.stringify(PrioritizA),
      httpOptions
    );
  }

  // get customer list...
  getCustomerList(): Observable<any[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':'bearer 8862d1f2-87df-4e22-a70f-561f8da4bcf5'
      })
    }

    return this.httpClient.get<any[]>(`http://localhost:8081/api/getContactList`, httpOptions);
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json',
    //     'Authorization':'bearer 9c1fc18d-0d89-491a-b713-e6972e2c2e69'
    //   })
    // }
    // return this.httpClient.get<any[]>(
    //   `http://localhost:8081/api/getContactList`,
    //   JSON.stringify(retirementDetails),
    //   httpOptions
    // );
    
  }

}





