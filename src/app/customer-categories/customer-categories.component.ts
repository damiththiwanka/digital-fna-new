import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from "./../api.service";
import { FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-customer-categories',
  templateUrl: './customer-categories.component.html',
  styleUrls: ['./customer-categories.component.scss']
})
export class CustomerCategoriesComponent implements OnInit {
  public loading = false;
  customerSelectForm: any;
  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  public userId: any=[];

  constructor(
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private router: Router,
    private apiService: ApiService,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService
  ) { }

  ngOnInit() {
    this.initForm();
    this.getCustomerList();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  initForm() {
    this.customerSelectForm = this.formBuilder.group({
      selectCustomerNumber: ['', Validators.required]
    })
  }

  persontype(type) {
    if(this.customerSelectForm.valid) {
      this.router.navigate(['customer'], { queryParams: { categoryNumber: type } });
    } else {
      this.openSnackBar("Please Select a Customer !", "OK");
    }
    
  }

  getSelectedCustomer(customer) {
    console.log(customer.option.value);
    this.storage.set("userId", customer.option.value);
    this.userId["userId"]= this.storage.get("userId");
  }

  getCustomerList() {
    this.loading = true;
    this.apiService.getCustomerList()
      .subscribe(response => {
        if (response) {
          this.loading = true;
          this.options = response;
          this.filteredOptions = this.myControl.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter(value))
            );
        }

      })
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter((option: any) => option.contactNo.toLowerCase().includes(filterValue));
  }

}
