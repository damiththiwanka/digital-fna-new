export interface cd_family_details{
    id:String ;  
    age:String;  
    contactNo: String;     
    gender: String;   
    occupation: String;  
    updatedDateTime:String;    
    addedDateTime: String;   
    spouseId:String;   
    spouseAge: String;   
    spouseGender: String;   
    spouseOccupation: String;    
    spouseIncome: String;    
    expensesId: String;     
    lifeInsurance: String;  
    retirement: String;  
    myResident: String;    
    disabilityincome: String;   
    hospitalexpenses: String; 
    monthlyIncomeMainLife: String; 
    criticalIllnessfund: String; 
    monthlyIncomeSpouse: String; 
}