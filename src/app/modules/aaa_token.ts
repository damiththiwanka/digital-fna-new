export interface aaa_token {
    status: number;
    message: String;
    token: String;
}