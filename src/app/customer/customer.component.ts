import { Component, OnInit, Pipe, Inject, ViewChild } from '@angular/core';
import { formatCurrency } from "@angular/common";
import { CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { max } from 'rxjs/operators';
import { SourceListMap } from 'source-list-map';
import { Chart } from "chart.js";
import { Color } from 'ng2-charts';
import { MergeMapSubscriber } from 'rxjs/internal/operators/mergeMap';
import { ApiService } from "../api.service";
import { cd_token_ausys } from "../modules/token_ausys"
import { aaa_token } from '../modules/aaa_token';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute } from '@angular/router';
import { LOCAL_STORAGE, WebStorageService } from 'ngx-webstorage-service';



export interface Ocupation {
  value: string;
  viewValue: string;
}
export interface Member {
  title: string;
  image: string;
}

export interface PrioritizingArray {
  id: String;
  customer_id: String;
  priority: String;
  summary: String;
  addedDateTime: String;
  updatedDateTime: String;
}

@Pipe({
  name: 'noComma'
})


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  public loading = false;

  // tslint:disable-next-line:variable-name

  public category: any;
  public categoryType: any;
  public userId: any = [];

  constructor(
    private _formBuilder: FormBuilder,
    private apiService: ApiService,
    private route: ActivatedRoute,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService
  ) { }

  public labels = ['50000', '75000', '100000', '200000'];
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  forthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;
  selectedValue: string;
  selectedCar: string;

  amount_father: number = 0;
  amount_mother: number = 0;
  age_father: number = 25;
  age_mother: number = 25;
  age_boy01: number = 12;
  age_girl01: number = 12;
  father: boolean = false;
  mother: boolean = false;
  boy01: boolean = false;
  girl01: boolean = false;

  hospital_expenses: number = 0;
  Critical_Illness: number = 0;
  Disability_expenses: number = 0;
  Retirement_fund: number = 0;
  Life_Insurance: number = 0;

  panelOpenState = false;

  tokenesponce: cd_token_ausys;

  motherFatherSaveObj = {
    id: "",
    age: "",
    contactNo: "",
    gender: "",
    occupation: "",
    updatedDateTime: "",
    addedDateTime: "",
    spouseId: "",
    spouseAge: "",
    spouseGender: "",
    spouseOccupation: "",
    spouseIncome: "",
    expensesId: "",
    lifeInsurance: "",
    retirement: "",
    myResident: "",
    disabilityincome: "",
    hospitalexpenses: "",
    monthlyIncomeMainLife: "",
    criticalIllnessfund: "",
    monthlyIncomeSpouse: ""
  }

  financialStreangthSaveObj = {
    id: "",
    customerId: "",
    cashOtherAssets: "",
    debts: "",
    creditCard: "",
    autoLoan: "",
    mortgageOthers: "",
    mortgageResidence: "",
    insuranceCashValue: "",
    pensionFund: "",
    businessInterest: "",
    realEstateNoResident: "",
    realEstateResident: "",
    otherDebtObligations: "",
    addedDateTime: null,
    updatedDateTime: null
  }

  cashAndIncomeSaveObj = {
    id: "",
    customer_id: "",
    immediate_money_fund: "",
    debt_liquidation: "",
    emergency_fund: "",
    educational_fund: "",
    mortgage_residence: "",
    if_rent: "",
    sub_total: "",
    cash_other_liquid_assets: "",
    existing_life_insurance: "",
    capital_required: "",
    assumed_interest_rate: "",
    total_annual_income_shortage: "",
    other_annual_incomes: "",
    annual_guaranteed_incomes: "",
    proposing_income_objective_to_family: "",
    addedDateTime: null,
    updatedDateTime: null,
    contactNo: ""
  }

  retirementAndInvestmentSaveObj = {
    id: "",
    customerId: "",
    focusAnnualIncomeRetirement: "",
    assumeInterestRate: "",
    incomeAtRetirement: "",
    totalGuaranteedIncome: "",
    incomeNeededFromSavings: "",
    fundBuildingPlan: "",
    otherSavings: "",
    investmentLumpSumMoney: "",
    existingLifeInsurance: "",
    capitalRequired: "",
    assumedInterestRate: "",
    totalAnnualIncomeShortage: "",
    otherAnnualIncomes: "",
    annualGuaranteedIncomes: "",
    incomeObjectiveToFamily: "",
    addedDateTime: null,
    updatedDateTime: null,
    contactNo: ""
  }


  Familyfather: Member = { title: 'Father', image: 'assets/img/main.png' }
  Familymother: Member = { title: 'Mother', image: 'assets/img/wife.png' }
  Familyboy01: Member = { title: 'Boy 01', image: 'assets/img/child_m.png' }
  Familygirl01: Member = { title: 'Girl 01', image: 'assets/img/child_f.png' }

  family: Member[] = [
    this.Familyfather,
    this.Familymother,
    this.Familyboy01,
    this.Familygirl01
    // ,
    // {
    //   title: 'Boy 02',
    //   image: 'assets/img/child_m.png'
    // },
    // {
    //   title: 'Girl 02',
    //   image: 'assets/img/child_f.png'
    // }
    // ,
    // {
    //   title: 'Boy 03',
    //   image: 'assets/img/child_m.png'
    // },
    // {
    //   title: 'Girl 03',
    //   image: 'assets/img/child_f.png'
    // }
  ];

  //080610 token number

  done: Member[] = [

  ];

  Prioritiz: Number[] = [8, 7, 6, 5, 4, 3, 2, 1];

  PrioritizA: PrioritizingArray[] = [];

  ocupation: Ocupation[] = [
    { value: 'SE', viewValue: 'Software Engineer' },
    { value: 'ME', viewValue: 'Mechanical Engineer' },
    { value: 'EE', viewValue: 'Engineer' }
  ];

  // Financial Strenth ++++++++

  Cash_Other_assets: number = 0;
  Real_Estate_Resident: number = 0;
  Real_Estate_No_Resident: number = 0;
  Business_Interest: number = 0;
  Pension_Fund: number = 0;
  Insurance_cash_value: number = 0;

  Mortgage_Residence: number = 0;
  Mortgage_Others: number = 0;
  Auto_Loan: number = 0;
  Credit_Card: number = 0;
  Debts: number = 0;
  Other_debt_obligations: number = 0;

  MFS_Assets: number = 0;
  MFS_Liabilities: number = 0;
  MFS_Balance: number = 0;


  // CHART ++++++++

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };

  public mbarChartLabels: string[] = ['Assets', 'liabilities'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = false;

  public barChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105,159,177,0.2)',
      borderColor: 'rgba(105,159,177,1)',
      pointBackgroundColor: 'rgba(105,159,177,1)',
      pointBorderColor: '#fafafa',
      pointHoverBackgroundColor: '#fafafa',
      pointHoverBorderColor: 'rgba(105,159,177)'
    },
    {
      backgroundColor: 'rgba(77,20,96,0.3)',
      borderColor: 'rgba(77,20,96,1)',
      pointBackgroundColor: 'rgba(77,20,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,20,96,1)'
    }
  ];
  public barChartData: any[] = [
    { data: [0, 0] }
  ];
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  //Chart --------
  // Financial Strenth ----------

  //Cash & Income Need ++++++++++
  Immediate_Money_Fund: number = 0;
  Immediate_Money_Fund_times: number = 6;

  Debt_liquidation: number = 0;
  Debt_liquidation_times: number = 6;

  Emergency_Fund: number = 0;
  Emergency_Fund_times: number = 3;

  Educational_Fund: number = 10;

  Mortgage_Or_Rent: number = 0;

  rentOption = false;
  CD_rent_amount: number = 5000;

  color = 'primary';

  IncomeObjective: number = 70;
  AnnualGuaranteedincomes: number = 0;
  AssumeInterest: number = 5;
  OtherAnnualincomes: number = 0;
  IncomeNeed: number = 0;
  IncomeNeedString: String;
  Cashneed: number = 0;
  CashNeedString: String;
  IncomeNeedTemp: number = 0;
  ProtectFamilyFinancial: number = 0;
  ProtectFamilyFinancialString: String = "";

  //Cash & Income Need ----------

  // Retirement & Investment +++++++++++
  FocusAnnualIncomeatRetirement: number = 0;
  FocusAnnualIncomeatRetirementString: String = "";

  Assumeinterestrate: number = 5;

  Capitalneeded: number = 0;
  CapitalneededString: String = "";

  Childreneducationfund: number = 2000000;
  ChildreneducationfundString: String = "";

  OtherSavings: number = 0;
  OtherSavingsString: String = "";

  InvestmentLumpSum: number = 0;
  InvestmentLumpSumString: String = "";

  Age: number = 25;
  RAge: number = 55;
  Remaining_Years: number = 30;

  FinancialSecurityValue: any = "Not set";
  FamilyHealthExpensesValue: any = "Not set";
  CriticalIllnessexpensesValue: any = "Not set";
  DisabilityexpensesValue: any = "Not set";
  RetirementIncomeFundValue: any = "Not set";
  ChildrenHigherEducationValue: any = "Not set";
  OtherSavingsValue: any = "Not set";
  InvestmentsValue: any = "Not set";

  // Retirement & Investment -----------
  @ViewChild('stepper', { static: false }) private mystepper: MatStepper;

  ngOnInit() {
    this.userId["userId"] = this.storage.get("userId");

    // this.done.push({
    //   title: 'Mother',
    //   image: 'assets/img/wife.png'
    // });
    this.Asys_login_post();
    //this.AAA_login();
    // this.family.splice(1,1);

    // if (this.father) {
    //   this.firstFormGroup = this._formBuilder.group({
    //     selectFatherOccupation: ['', Validators.required]
    //   });
    // }

    // if (this.mother) {
    //   this.firstFormGroup = this._formBuilder.group({
    //     selectMotherOccupation: ['', Validators.required]
    //   });
    // }

    // if (this.father && this.mother) {
    //   this.firstFormGroup = this._formBuilder.group({
    //     selectFatherOccupation: ['', Validators.required],
    //     selectMotherOccupation: ['', Validators.required]
    //   });
    // }

    this.firstFormGroup = this._formBuilder.group({
      selectFatherOccupation: ['', Validators.required],
      selectMotherOccupation: ['', Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
    this.forthFormGroup = this._formBuilder.group({
      forthCtrl: ['', Validators.required]
    });
    this.fifthFormGroup = this._formBuilder.group({
      fifthCtrl: ['', Validators.required]
    });



    this.category = this.route
      .queryParams
      .subscribe(params => {
        this.categoryType = params;
      })

    switch (this.categoryType.categoryNumber) {
      case 'one':
        this.requestedMembers(this.Familyfather);
        this.father = true;
        break;
      case 'two':
        this.requestedMembers(this.Familyfather);
        this.requestedMembers(this.Familymother);
        this.father = true;
        this.mother = true;
        break;
      case 'three':
        this.requestedMembers(this.Familyfather);
        this.requestedMembers(this.Familymother);
        this.requestedMembers(this.Familyboy01);
        this.father = true;
        this.mother = true;
        this.boy01 = true;
        break;
      case 'four':
        this.requestedMembers(this.Familyfather);
        this.requestedMembers(this.Familymother);
        this.father = true;
        this.mother = true;
        break;
      default:
        break;
    }
  }

  Asys_login_post() {
    this.apiService.getAusysToken().subscribe(data => {
    });
  }

  //=============== Customer Details ===========

  requestedMembers(member: Member) {
    this.done.push(member);
    var index = this.family.findIndex(familyMember => familyMember === member);
    this.family.splice(index, 1);
  }

  summarySubmit() {
    this.loading = true;
    this.PrioritizA = [];

    this.PrioritizA.push(
      { id: "", customer_id: this.userId.userId.toString(), priority: this.FinancialSecurityValue.toString(), summary: "FinancialSecurity", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.FamilyHealthExpensesValue.toString(), summary: "FamilyHealthExpenses", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.CriticalIllnessexpensesValue.toString(), summary: "CriticalIllnessexpenses", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.DisabilityexpensesValue.toString(), summary: "Disabilityexpenses", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.RetirementIncomeFundValue.toString(), summary: "RetirementIncomeFund", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.ChildrenHigherEducationValue.toString(), summary: "ChildrenHigherEducation", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.OtherSavingsValue.toString(), summary: "OtherSavings", addedDateTime: "", updatedDateTime: "" },
      { id: "", customer_id: this.userId.userId.toString(), priority: this.InvestmentsValue.toString(), summary: "Investments", addedDateTime: "", updatedDateTime: "" }
    );

    console.log(this.PrioritizA);

    this.apiService.saveSummaryDetails(this.userId.userId.toString(), this.PrioritizA)
      .subscribe(response => {
        this.loading = false;
      },
        err => {
          this.loading = false;
          console.log(err)
        })

  }

  setPriority(value, component) {

    console.log(value + component);
    /* 
    FinancialSecurityValue
    FamilyHealthExpensesValue
    CriticalIllnessexpensesValue
    DisabilityexpensesValue

    RetirementIncomeFund
    ChildrenHigherEducation
    OtherSavings
    Investments
    */

    switch (component) {
      case 'FinancialSecurity':
        this.FinancialSecurityValue = this.getPriorityValue(value);
        break;
      case 'FamilyHealthExpenses':
        this.FamilyHealthExpensesValue = this.getPriorityValue(value);
        break;
      case 'CriticalIllnessexpenses':
        this.CriticalIllnessexpensesValue = this.getPriorityValue(value);
        break;
      case 'Disabilityexpenses':
        this.DisabilityexpensesValue = this.getPriorityValue(value);
        break;

      case 'RetirementIncomeFund':
        this.RetirementIncomeFundValue = this.getPriorityValue(value);
        break;
      case 'ChildrenHigherEducation':
        this.ChildrenHigherEducationValue = this.getPriorityValue(value);
        break;
      case 'OtherSavings':
        this.OtherSavingsValue = this.getPriorityValue(value);
        break;
      case 'Investments':
        this.InvestmentsValue = this.getPriorityValue(value);
        break;
      default:
        break;
    }
  }

  getPriorityValue(value) {
    if (value == 'Not set') {
      return this.Prioritiz.pop();
    } else {
      this.Prioritiz.push(value);
      return 'Not set';
    }
    console.log(value);

  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      if (event.container.id == "cdk-01") {
        var Selecttype = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title;
        if (Selecttype === "Father") {
          this.father = true;
        }
        if (Selecttype === "Mother") {
          this.mother = true;
        }
        if (Selecttype === "Boy 01") {
          this.boy01 = true;
        }
        if (Selecttype === "Girl 01") {
          this.girl01 = true;
        }
      } else if (event.container.id == "cdk-02") {
        var Selecttype = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title;
        if (Selecttype === "Father") {
          this.father = false;
        }
        if (Selecttype === "Mother") {
          this.mother = false;
        }
        if (Selecttype === "Boy 01") {
          this.boy01 = false;
        }
        if (Selecttype === "Girl 01") {
          this.girl01 = false;
        }
      }
    }
  }

  formatLabel(value: number) {
    return Math.round(value * 10000);
  }

  SalaryLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }
    return value;
  }

  changeIncome(event, type) {
    if (type == "F") {
      this.amount_father = Math.round(event.value / 1000);
    }
    if (type == "M") {
      this.amount_mother = Math.round(event.value / 1000);
    }

  }
  OnChangeRent(event) {
    this.rentOption = event.checked;
  }

  changeAge(event, type) {
    if (type == "F") {
      this.age_father = event.value;
      this.Age = this.age_father;
    }
    if (type == "M") {
      this.age_mother = event.value;
    }
    if (type == "B1") {
      this.age_boy01 = event.value;
    }
    if (type == "G1") {
      this.age_girl01 = event.value;
    }
  }

  changeHotel(event) {
    this.hospital_expenses = Math.round(event.value / 1000);
  }

  changeCritical_Illness(event) {
    this.Critical_Illness = Math.round(event.value / 1000);
  }

  changeDisability_expenses(event) {
    this.Disability_expenses = Math.round(event.value / 1000);
  }

  changeRetirement_fund(event) {
    this.Retirement_fund = Math.round(event.value / 1000);
  }

  changeLife_Insurance(event) {
    this.Life_Insurance = Math.round(event.value / 1000);
  }
  //=============== END Customer Details ===========

  //=============== Financial Strength Details ===========

  changeCash_Other_assets(event) {
    this.Cash_Other_assets = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeReal_Estate_Resident(event) {
    this.Real_Estate_Resident = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeReal_Estate_No_Resident(event) {
    this.Real_Estate_No_Resident = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeBusiness_Interest(event) {
    this.Business_Interest = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changePension_Fund(event) {
    this.Pension_Fund = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeInsurance_cash_value(event) {
    this.Insurance_cash_value = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeMortgage_Residence(event) {
    this.Mortgage_Residence = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeMortgage_Others(event) {
    this.Mortgage_Others = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeAuto_Loan(event) {
    this.Auto_Loan = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeCredit_Card(event) {
    this.Credit_Card = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeDebts(event) {
    this.Debts = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeOther_debt_obligations(event) {
    this.Other_debt_obligations = Math.round(event.value / 1000);
    this.changeChartValues();
  }

  changeChartValues(): void {
    this.MFS_Assets = this.Cash_Other_assets +
      this.Real_Estate_Resident +
      this.Real_Estate_No_Resident +
      this.Business_Interest +
      this.Pension_Fund +
      this.Insurance_cash_value;

    this.MFS_Liabilities = this.Mortgage_Residence +
      this.Mortgage_Others +
      this.Auto_Loan +
      this.Credit_Card +
      this.Debts +
      this.Other_debt_obligations;

    this.MFS_Balance = this.MFS_Assets - this.MFS_Liabilities;
    // let data = [this.MFS_Assets, this.MFS_Liabilities];
    this.barChartData[0].data = [this.MFS_Assets, this.MFS_Liabilities];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    // this.barChartData[0].data = data;
    this.barChartData = clone;
  }

  //Cash & Income Need ++++++++++
  CIN_init() {
    this.Immediate_Money_Fund = this.amount_father * 6;
    this.Debt_liquidation = this.amount_father * 6;
    this.Emergency_Fund = this.amount_father * 3;
    this.Educational_Fund = 10;
    this.changeCashNeed();
    this.changeIncomeNeed();
    this.changeProtectFamilyFinancial();
  }

  changeImmediate_Money_Fund(event) {
    this.Immediate_Money_Fund_times = event.value;
    this.Immediate_Money_Fund = this.amount_father * event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeDebt_liquidation(event) {
    this.Debt_liquidation_times = event.value;
    this.Debt_liquidation = this.amount_father * event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeEmergency_Fund(event) {
    this.Emergency_Fund_times = event.value;
    this.Emergency_Fund = this.amount_father * event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeEducational_Fund(event) {
    this.Educational_Fund = Math.round(event.value / 1000000);
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  OnChangeRentAmoutn(value) {
    this.CD_rent_amount = value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeIncome_Objective(event) {
    this.IncomeObjective = event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();

  }

  OnChangeAnnualGuaranteedincomes(value) {
    this.AnnualGuaranteedincomes = value
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeAssumeInterest(event) {
    this.AssumeInterest = event.value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  OnChangeGuaranteedOtherIncomeAmount(value) {
    this.OtherAnnualincomes = value;
    this.changeIncomeNeed();
    this.changeCashNeed();
  }

  changeIncomeNeed() {
    var IncomeObjectiveTemp = (((this.amount_father + this.amount_mother) * 12000) * (this.IncomeObjective / 100));
    var IncomeShortageTEMP = (this.AnnualGuaranteedincomes + this.OtherAnnualincomes);
    var IncomeShortage = 0;
    if (IncomeObjectiveTemp > IncomeShortageTEMP) {
      IncomeShortage = (IncomeObjectiveTemp - IncomeShortageTEMP);
    } else if (IncomeObjectiveTemp < IncomeShortageTEMP) {
      IncomeShortage = (IncomeShortageTEMP - IncomeObjectiveTemp);
    } else {
      IncomeShortage = 0;
    }

    this.IncomeNeedTemp = IncomeShortage * (100 / this.AssumeInterest);
    this.IncomeNeedString = formatCurrency(this.IncomeNeedTemp, 'en-US', '', 'LKR');
    this.changeProtectFamilyFinancial();
  }

  changeCashNeed() {
    this.Cashneed = ((this.Immediate_Money_Fund + this.Debt_liquidation + this.Emergency_Fund + (this.Educational_Fund * 1000)) * 1000 + (this.rentOption ? (this.CD_rent_amount * 120) : (this.Mortgage_Residence * 1000))) - ((this.Cash_Other_assets + this.Life_Insurance) * 1000)
    this.CashNeedString = formatCurrency(this.Cashneed, 'en-US', '', 'LKR');
    this.changeProtectFamilyFinancial();
  }

  changeProtectFamilyFinancial() {
    this.ProtectFamilyFinancial = this.IncomeNeedTemp + this.Cashneed;
    this.ProtectFamilyFinancialString = formatCurrency(this.ProtectFamilyFinancial, 'en-US', '', 'LKR');
  }

  OnChangeFocusAnnualIncomeatRetirement(value) {
    this.FocusAnnualIncomeatRetirement = value
    this.changeCapitalneeded();
  }

  changeAssumeinterestrate(event) {
    this.Assumeinterestrate = event.value;
    this.changeCapitalneeded();
  }

  changeCapitalneeded() {
    this.Capitalneeded = this.FocusAnnualIncomeatRetirement * (100 / this.Assumeinterestrate)
    this.CapitalneededString = formatCurrency(this.Capitalneeded, 'en-US', '', 'LKR');

  }

  changeChildreneducationfund(event) {
    this.Childreneducationfund = event.value;
    this.ChildreneducationfundString = formatCurrency(this.Childreneducationfund, 'en-US', '', 'LKR');
  }

  changeOtherSavings(event) {
    this.OtherSavings = event.value;
    this.OtherSavingsString = formatCurrency(this.OtherSavings, 'en-US', '', 'LKR');
  }

  transform(OtherSavingsString: number): string {
    if (OtherSavingsString !== undefined && OtherSavingsString !== null) {
      // here we just remove the commas from value
      return OtherSavingsString.toString().replace(/,/g, "");
    } else {
      return "";
    }
  }

  changeInvestmentLumpSum(event) {
    this.InvestmentLumpSum = event.value;
    this.InvestmentLumpSumString = formatCurrency(this.InvestmentLumpSum, 'en-US', '', 'LKR');
  }
  changeRAge(event) {
    this.RAge = event.value;
    this.Remaining_Years = this.RAge - this.Age;
  }



  nextSaveCustomerDetails() {
    this.saveCustomer(this.firstFormGroup.value);
  }

  saveCustomer(values) {
    this.loading = true;
    this.motherFatherSaveObj.id = "",
      this.motherFatherSaveObj.age = this.age_father.toString(),
      this.motherFatherSaveObj.contactNo = this.userId.userId,
      this.motherFatherSaveObj.gender = "M",
      this.motherFatherSaveObj.occupation = values.selectFatherOccupation,
      this.motherFatherSaveObj.updatedDateTime = "",
      this.motherFatherSaveObj.addedDateTime = "",
      this.motherFatherSaveObj.spouseId = "",
      this.motherFatherSaveObj.spouseAge = this.age_mother.toString(),
      this.motherFatherSaveObj.spouseGender = "F",
      this.motherFatherSaveObj.spouseOccupation = values.selectMotherOccupation,
      this.motherFatherSaveObj.spouseIncome = this.amount_mother.toString(),
      this.motherFatherSaveObj.expensesId = "",
      this.motherFatherSaveObj.lifeInsurance = this.Life_Insurance.toString(),
      this.motherFatherSaveObj.retirement = this.Retirement_fund.toString(),
      this.motherFatherSaveObj.myResident = this.CD_rent_amount.toString() || "Own House",
      this.motherFatherSaveObj.disabilityincome = this.Disability_expenses.toString(),
      this.motherFatherSaveObj.hospitalexpenses = this.hospital_expenses.toString(),
      this.motherFatherSaveObj.monthlyIncomeMainLife = this.amount_father.toString(),
      this.motherFatherSaveObj.criticalIllnessfund = this.Critical_Illness.toString(),
      this.motherFatherSaveObj.monthlyIncomeSpouse = this.amount_mother.toString()

    this.apiService.saveCustomer(this.motherFatherSaveObj)
      .subscribe(response => {
        this.loading = false;
        console.log(response);
      },
        err => {
          this.loading = false;
          console.log(err)
        })
  }

  nextSaveFinancialStreangth() {
    this.loading = true;
    this.financialStreangthSaveObj.id = ""
    this.financialStreangthSaveObj.customerId = this.userId.userId;
    this.financialStreangthSaveObj.cashOtherAssets = this.Cash_Other_assets.toString();
    this.financialStreangthSaveObj.debts = this.Debts.toString();
    this.financialStreangthSaveObj.creditCard = this.Credit_Card.toString();
    this.financialStreangthSaveObj.autoLoan = this.Auto_Loan.toString();
    this.financialStreangthSaveObj.mortgageOthers = this.Mortgage_Others.toString();
    this.financialStreangthSaveObj.mortgageResidence = this.Mortgage_Residence.toString();
    this.financialStreangthSaveObj.insuranceCashValue = this.Insurance_cash_value.toString();
    this.financialStreangthSaveObj.pensionFund = this.Pension_Fund.toString();
    this.financialStreangthSaveObj.businessInterest = this.Business_Interest.toString();
    this.financialStreangthSaveObj.realEstateNoResident = this.Real_Estate_No_Resident.toString();
    this.financialStreangthSaveObj.realEstateResident = this.Real_Estate_Resident.toString();
    this.financialStreangthSaveObj.otherDebtObligations = this.Other_debt_obligations.toString();
    this.financialStreangthSaveObj.addedDateTime = ""
    this.financialStreangthSaveObj.updatedDateTime = ""

    this.apiService.saveFinancialStreangthDetails(this.financialStreangthSaveObj)
      .subscribe(response => {
        this.loading = false;
      },
        err => {
          this.loading = false;
          console.log(err)
        })
  }

  nextSaveCashAndInconeNeeddetails() {
    this.loading = true;
    this.cashAndIncomeSaveObj.id = "";
    this.cashAndIncomeSaveObj.customer_id = this.userId.userId;
    this.cashAndIncomeSaveObj.immediate_money_fund = this.Immediate_Money_Fund.toString();
    this.cashAndIncomeSaveObj.debt_liquidation = this.Debt_liquidation.toString();
    this.cashAndIncomeSaveObj.emergency_fund = this.Emergency_Fund.toString();
    this.cashAndIncomeSaveObj.educational_fund = this.Educational_Fund.toString();
    if (this.rentOption) {
      this.cashAndIncomeSaveObj.if_rent = (this.CD_rent_amount * 120).toString();
      this.cashAndIncomeSaveObj.mortgage_residence = "0";
    }
    if (!this.rentOption) {
      this.cashAndIncomeSaveObj.mortgage_residence = (this.Mortgage_Residence * 1000).toString();
      this.cashAndIncomeSaveObj.if_rent = "0";
    }
    this.cashAndIncomeSaveObj.sub_total = ((this.Immediate_Money_Fund + this.Debt_liquidation + this.Emergency_Fund + (this.Educational_Fund * 1000)) * 1000 + (this.rentOption ? (this.CD_rent_amount * 120) : (this.Mortgage_Residence * 1000)) * 1) + "";
    this.cashAndIncomeSaveObj.cash_other_liquid_assets = (this.Cash_Other_assets * 1000).toString();
    this.cashAndIncomeSaveObj.existing_life_insurance = (this.Life_Insurance * 1000).toString();
    this.cashAndIncomeSaveObj.capital_required = ((this.Cash_Other_assets + this.Life_Insurance) * 1000).toString();
    this.cashAndIncomeSaveObj.assumed_interest_rate = this.AssumeInterest.toString();
    this.cashAndIncomeSaveObj.total_annual_income_shortage = (((((this.amount_father + this.amount_mother) * 1000) * 12) * this.IncomeObjective) / 100) - ((this.AnnualGuaranteedincomes * 1) + (this.OtherAnnualincomes * 1)) + "";
    this.cashAndIncomeSaveObj.other_annual_incomes = this.OtherAnnualincomes.toString();
    this.cashAndIncomeSaveObj.annual_guaranteed_incomes = this.AnnualGuaranteedincomes.toString();
    this.cashAndIncomeSaveObj.proposing_income_objective_to_family = (((((this.amount_father + this.amount_mother) * 1000) * 12) * this.IncomeObjective) / 100).toString();
    this.cashAndIncomeSaveObj.addedDateTime = "";
    this.cashAndIncomeSaveObj.updatedDateTime = "";
    this.cashAndIncomeSaveObj.contactNo = this.userId.userId;

    this.apiService.saveCashAndIncomeDetails(this.cashAndIncomeSaveObj)
      .subscribe(response => {
        this.loading = false;
      },
        err => {
          this.loading = false;
          console.log(err)
        })
  }

  nextSaveRetirementAndInvestmentDetails() {
    this.loading = true;
    this.retirementAndInvestmentSaveObj.id = "";
    this.retirementAndInvestmentSaveObj.customerId = this.userId.userId;
    this.retirementAndInvestmentSaveObj.focusAnnualIncomeRetirement = this.FocusAnnualIncomeatRetirement.toString();
    this.retirementAndInvestmentSaveObj.assumeInterestRate = this.Assumeinterestrate.toString();
    this.retirementAndInvestmentSaveObj.incomeAtRetirement = "0";
    this.retirementAndInvestmentSaveObj.totalGuaranteedIncome = "0";
    this.retirementAndInvestmentSaveObj.incomeNeededFromSavings = this.Capitalneeded - ((this.AnnualGuaranteedincomes * 1) + (this.OtherAnnualincomes * 1)) + "";
    this.retirementAndInvestmentSaveObj.fundBuildingPlan = this.ChildreneducationfundString.toString();
    this.retirementAndInvestmentSaveObj.otherSavings = this.OtherSavingsString.toString();
    this.retirementAndInvestmentSaveObj.investmentLumpSumMoney = this.InvestmentLumpSumString.toString();
    this.retirementAndInvestmentSaveObj.existingLifeInsurance = "0";
    this.retirementAndInvestmentSaveObj.capitalRequired = "0";
    this.retirementAndInvestmentSaveObj.totalAnnualIncomeShortage = "0";
    this.retirementAndInvestmentSaveObj.otherAnnualIncomes = "0";
    this.retirementAndInvestmentSaveObj.annualGuaranteedIncomes = (this.AnnualGuaranteedincomes * 1) + (this.OtherAnnualincomes * 1).toString();
    this.retirementAndInvestmentSaveObj.incomeObjectiveToFamily = "0";
    this.retirementAndInvestmentSaveObj.addedDateTime = "";
    this.retirementAndInvestmentSaveObj.updatedDateTime = "";
    this.retirementAndInvestmentSaveObj.contactNo = this.userId.userId;

    if (this.retirementAndInvestmentSaveObj.fundBuildingPlan == "") {
      this.retirementAndInvestmentSaveObj.fundBuildingPlan = "0"
    }

    this.apiService.saveRetirementAndDetails(this.retirementAndInvestmentSaveObj)
      .subscribe(response => {
        this.loading = false;
      },
        err => {
          this.loading = false;
          console.log(err)
        })
  }
}

