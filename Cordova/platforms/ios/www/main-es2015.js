(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main-nav><router-outlet></router-outlet></main-nav>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/customer/customer.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/customer/customer.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--\n<button mat-raised-button (click)=\"isLinear = !isLinear\" id=\"toggle-linear\">\n    {{!isLinear ? 'Enable linear mode' : 'Disable linear mode'}}\n  </button>\n    -->\n\n<mat-horizontal-stepper [linear]=\"isLinear\" #stepper>\n  <mat-step [stepControl]=\"firstFormGroup\">\n    <form [formGroup]=\"firstFormGroup\">\n      <ng-template matStepLabel>Customer Details</ng-template>\n      <br />\n      <div class=\"row\">\n        <div class=\"col col-lg-3\" style=\"overflow: auto; height:450px;\">\n          <mat-card class=\"familycard\" *ngIf=\"father\">\n            <div class=\"row\">\n              <div class=\"col-4 col-img\">\n                <img src=\"assets/img/main.png\">\n              </div>\n              <div class=\"col-8 cardcontent\">\n                <div class=\"col cardcontent\">\n                  <mat-form-field>\n                    <mat-label>Occupation</mat-label>\n                    <mat-select>\n                      <mat-option>--</mat-option>\n                      <mat-option value=\"eng\">\n                        Enginere\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n\n                  <mat-label class=\"Income-lable\">Income {{amount_father}}</mat-label>\n                  <mat-slider (input)=\"changeIncome($event,'F')\" class=\"salary\" thumbLabel [displayWith]=\"SalaryLabel\"\n                    step=\"10000\" value=\"50000\" tickInterval=\"Auto\" min=\"40000\" max=\"500000\">\n                  </mat-slider>\n                  <!-- <mat-error *ngIf=\"animalControl.hasError('required')\">Please choose an animal</mat-error> -->\n                  <!-- <mat-hint>it dosent matter</mat-hint> -->\n                  <mat-label class=\"Income-lable\">Age {{age_father}}</mat-label>\n                  <mat-slider (input)=\"changeAge($event,'F')\" class=\"age\" thumbLabel step=\"1\" value=\"25\"\n                    tickInterval=\"Auto\" min=\"20\" max=\"65\">\n                  </mat-slider>\n                </div>\n              </div>\n            </div>\n          </mat-card>\n\n          <mat-card class=\"familycard\" *ngIf=\"mother\">\n            <div class=\"row\">\n              <div class=\"col-4 col-img\">\n                <img src=\"assets/img/wife.png\">\n              </div>\n              <div class=\"col-8 cardcontent\">\n                <div class=\"col cardcontent\">\n                  <mat-form-field>\n                    <mat-label>Occupation</mat-label>\n                    <mat-select>\n                      <mat-option>--</mat-option>\n                      <mat-option value=\"eng\">\n                        Enginere\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n\n                  <mat-label class=\"Income-lable\">Income {{amount_mother}}</mat-label>\n                  <mat-slider (input)=\"changeIncome($event,'M')\" class=\"salary\" thumbLabel [displayWith]=\"SalaryLabel\"\n                    step=\"10000\" value=\"50000\" tickInterval=\"Auto\" min=\"40000\" max=\"500000\">\n                  </mat-slider>\n                  <!-- <mat-error *ngIf=\"animalControl.hasError('required')\">Please choose an animal</mat-error> -->\n                  <!-- <mat-hint>it dosent matter</mat-hint> -->\n                  <mat-label class=\"Income-lable\">Age {{age_mother}}</mat-label>\n                  <mat-slider (input)=\"changeAge($event,'M')\" class=\"age\" thumbLabel step=\"1\" value=\"25\"\n                    tickInterval=\"Auto\" min=\"20\" max=\"65\">\n                  </mat-slider>\n                </div>\n              </div>\n            </div>\n          </mat-card>\n\n          <mat-card class=\"familycard\" *ngIf=\"boy01\">\n            <div class=\"row\">\n              <div class=\"col-4 col-img\">\n                <img src=\"assets/img/child_m.png\">\n              </div>\n              <div class=\"col-8 cardcontent\">\n                <div class=\"col cardcontent\">\n                  <mat-label class=\"Income-lable\">Age {{age_boy01}}</mat-label>\n                  <mat-slider (input)=\"changeAge($event,'B1')\" class=\"age\" thumbLabel step=\"1\" value=\"5\"\n                    tickInterval=\"Auto\" min=\"1\" max=\"30\">\n                  </mat-slider>\n                </div>\n              </div>\n            </div>\n          </mat-card>\n\n          <mat-card class=\"familycard\" *ngIf=\"girl01\">\n            <div class=\"row\">\n              <div class=\"col-4 col-img\">\n                <img src=\"assets/img/child_f.png\">\n              </div>\n              <div class=\"col-8 cardcontent\">\n                <div class=\"col cardcontent\">\n                  <mat-label class=\"Income-lable\">Age {{age_girl01}}</mat-label>\n                  <mat-slider (input)=\"changeAge($event,'G1')\" class=\"age\" thumbLabel step=\"1\" value=\"4\"\n                    tickInterval=\"Auto\" min=\"1\" max=\"30\">\n                  </mat-slider>\n                </div>\n              </div>\n            </div>\n          </mat-card>\n        </div>\n\n        <div class=\"col col-lg-6 \">\n          <!-- <h2>Done</h2> -->\n          <div cdkDropList #doneList=\"cdkDropList\" cdkDropListOrientation=\"horizontal\" [cdkDropListData]=\"done\"\n            [cdkDropListConnectedTo]=\"[todoList]\" class=\"example-list1\" (cdkDropListDropped)=\" drop($event)\">\n            <div class=\"example-box1 \" *ngFor=\"let dones of done\" cdkDrag>\n              <img [src]=\"dones.image\" width=\"Auto\" [alt]=\"dones.title\">\n            </div>\n          </div>\n          <br />\n          <div cdkDropList cdkDropListSortingDisabled #todoList=\"cdkDropList\" [cdkDropListData]=\"family\"\n            [cdkDropListConnectedTo]=\"[doneList]\" class=\"example-list float-left\" (cdkDropListDropped)=\"drop($event)\">\n            <div class=\"example-box\" *ngFor=\"let member of family\" cdkDrag>{{member.title}}\n              <img *cdkDragPreview class=\".cdk-drag-animating\" [src]=\"member.image\" [alt]=\"member.title\">\n            </div>\n          </div>\n        </div>\n\n        <div class=\"col col-lg-3\">\n          <mat-accordion >\n            <p>Immediate Family expenses</p>\n            <mat-expansion-panel class=\"expansion\" style=\"background-color: rgb(238, 224, 202) !important;\">\n              <mat-expansion-panel-header>\n                <mat-panel-title>\n                  Hospital expenses {{hotel_expenses}}\n                </mat-panel-title>\n                <!-- <mat-panel-description>\n                Currently I am {{panelOpenState ? 'open' : 'closed'}}\n              </mat-panel-description> -->\n              </mat-expansion-panel-header>\n              <mat-label class=\"Income-lable\">Value </mat-label>\n              <mat-slider (input)=\"changeHotel($event)\" class=\"hotel_expenses\" thumbLabel step=\"10000\" value=\"40000\"\n                tickInterval=\"Auto\" min=\"10000\" max=\"1000000\">\n              </mat-slider>\n            </mat-expansion-panel>\n\n            <mat-expansion-panel style=\"background-color: rgb(238, 232, 150) !important;\">\n              <mat-expansion-panel-header>\n                <mat-panel-title>\n                  Critical Illness fund {{Critical_Illness}}\n                </mat-panel-title>\n                <!-- <mat-panel-description>\n                  Currently I am {{panelOpenState ? 'open' : 'closed'}}\n                </mat-panel-description> -->\n              </mat-expansion-panel-header>\n              <mat-label class=\"Income-lable\">Value </mat-label>\n              <mat-slider (input)=\"changeCritical_Illness($event)\" class=\"Critical_Illness\" thumbLabel step=\"10000\"\n                value=\"40000\" tickInterval=\"Auto\" min=\"10000\" max=\"1000000\">\n              </mat-slider>\n            </mat-expansion-panel>\n\n            <mat-expansion-panel style=\"background-color: rgb(223, 238, 183) !important;\">\n              <mat-expansion-panel-header>\n                <mat-panel-title>\n                    Disability expenses  {{Disability_expenses}}\n                </mat-panel-title>\n                <!-- <mat-panel-description>\n                    Currently I am {{panelOpenState ? 'open' : 'closed'}}\n                  </mat-panel-description> -->\n              </mat-expansion-panel-header>\n              <mat-label class=\"Income-lable\">Value </mat-label>\n              <mat-slider (input)=\"changeDisability_expenses($event)\" class=\"Disability_expenses\" thumbLabel step=\"10000\"\n                value=\"40000\" tickInterval=\"Auto\" min=\"10000\" max=\"1000000\">\n              </mat-slider>\n            </mat-expansion-panel>\n          </mat-accordion>\n<br>\n          <mat-accordion>\n              <mat-expansion-panel style=\"background-color: cornflowerblue !important;\">\n                  <mat-expansion-panel-header>\n                    <mat-panel-title>\n                        Retirement Fund  {{Retirement_fund}}\n                    </mat-panel-title>\n                    <!-- <mat-panel-description>\n                        Currently I am {{panelOpenState ? 'open' : 'closed'}}\n                      </mat-panel-description> -->\n                  </mat-expansion-panel-header>\n                  <mat-label class=\"Income-lable\">Value </mat-label>\n                  <mat-slider (input)=\"changeRetirement_fund($event)\" class=\"Retirement_fund\" thumbLabel step=\"10000\"\n                    value=\"40000\" tickInterval=\"Auto\" min=\"10000\" max=\"1000000\">\n                  </mat-slider>\n                </mat-expansion-panel>\n          </mat-accordion>\n\n          <br>\n          <mat-accordion>\n              <mat-expansion-panel style=\"background-color: rgb(209, 98, 120)!important;\">\n                  <mat-expansion-panel-header>\n                    <mat-panel-title>\n                        Life Insurance {{Life_Insurance}}\n                    </mat-panel-title>\n                    <!-- <mat-panel-description>\n                        Currently I am {{panelOpenState ? 'open' : 'closed'}}\n                      </mat-panel-description> -->\n                  </mat-expansion-panel-header>\n                  <mat-label class=\"Income-lable\">Value </mat-label>\n                  <mat-slider (input)=\"changeLife_Insurance($event)\" class=\"Life_Insurance\" thumbLabel step=\"10000\"\n                    value=\"40000\" tickInterval=\"Auto\" min=\"10000\" max=\"1000000\">\n                  </mat-slider>\n                </mat-expansion-panel>\n          </mat-accordion>\n        </div>\n      </div>\n\n      <div class=\"col-12\">\n\n        <button mat-raised-button matStepperNext class=\"float-right\" color=\"primary\">Next</button>\n      </div>\n\n    </form>\n  </mat-step>\n\n  <mat-step [stepControl]=\"secondFormGroup\">\n    <form [formGroup]=\"secondFormGroup\">\n      <ng-template matStepLabel>My Financial Strength</ng-template>\n      <mat-form-field>\n        <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\n      </mat-form-field>\n      <div>\n        <button mat-button matStepperPrevious>Back</button>\n        <button mat-button matStepperNext>Next</button>\n      </div>\n    </form>\n  </mat-step>\n  <mat-step [stepControl]=\"thirdFormGroup\">\n    <form [formGroup]=\"thirdFormGroup\">\n      <ng-template matStepLabel>Cash & Income Need</ng-template>\n      <mat-form-field>\n        <input matInput placeholder=\"Address\" formControlName=\"thirdCtrl\" required>\n      </mat-form-field>\n      <div>\n        <button mat-button matStepperPrevious>Back</button>\n        <button mat-button matStepperNext>Next</button>\n      </div>\n    </form>\n  </mat-step>\n  <mat-step>\n    <ng-template matStepLabel>Retirement & Investment</ng-template>\n    You are now done.\n    <div>\n      <button mat-button matStepperPrevious>Back</button>\n      <button mat-button (click)=\"stepper.reset()\">Reset</button>\n    </div>\n  </mat-step>\n</mat-horizontal-stepper>\n\n<!-- <div class=\"example-container1\">\n        <h2>Family Data</h2>\n        <div>\n          <mat-card>\n            <mat-card-header>\n              <div mat-card-avatar class=\"example-header-image\"></div>\n              <mat-card-title>Father</mat-card-title>\n              <mat-card-subtitle>Main insured</mat-card-subtitle>\n            </mat-card-header>\n            <form>\n              <mat-form-field>\n                <mat-label>Occupation</mat-label>\n                <mat-select [(ngModel)]=\"selectedValue\" name=\"food\">\n                  <mat-option *ngFor=\"let food of foods\" [value]=\"food.value\">\n                    {{food.viewValue}}\n                  </mat-option>\n                </mat-select>\n              </mat-form-field>\n\n              <mat-slider min=\"5\" max=\"200\" step=\"2.5\" value=\"5\" thumbLabel [displayWith]=\"formatLabel\">\n              </mat-slider>\n              <p> Selected food: {{selectedValue}} </p>\n            </form>\n          </mat-card>\n        </div>\n      </div> -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/helper/family-card/family-card.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/helper/family-card/family-card.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Card component */\n<mat-card>\n  <mat-card-header>\n    <mat-card-title class=\"md-card-title\"></mat-card-title>\n  </mat-card-header>\n  <img mat-card-image [src]=\"cardImagePath\">\n  <mat-card-content>\n    <div class=\"card-text-content\">     \n    </div>\n  </mat-card-content>\n\n  <mat-card-actions fxLayout=\"row\" fxLayoutAlign=\"end center\">\n    <button md-button (click)=\"addProductToCart()\">ADAUGA IN COS</button>\n    <button md-button (click)=\"openDialog()\">DETALII</button>\n  </mat-card-actions>\n\n  <mat-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/main-nav/main-nav.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main-nav/main-nav.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-sidenav-container class=\"sidenav-container\">\n  <mat-sidenav \n  #drawer class=\"sidenav\" fixedInViewport\n      [attr.role]=\"(isHandset$ | async) ? 'dialog' : 'navigation'\"\n      [mode]=\"(isHandset$ | async) ? 'over' : 'side'\"\n      [opened]=\"(isHandset$ | async) === false\" color=\"primary\">\n    <mat-toolbar>Menu</mat-toolbar>\n    <mat-nav-list>\n      <a mat-list-item href=\"/customer\">Need Analysis</a>\n      <a mat-list-item href=\"#\">Link 2</a>\n      <a mat-list-item href=\"#\">Link 3</a>\n    </mat-nav-list>\n  </mat-sidenav>\n  <mat-sidenav-content>\n    <mat-toolbar color=\"primary\">\n      <button\n        type=\"button\"\n        aria-label=\"Toggle sidenav\"\n        mat-icon-button\n        (click)=\"drawer.toggle()\"\n        *ngIf=\"isHandset$ | async\">\n        <mat-icon aria-label=\"Side nav toggle icon\"> menu</mat-icon>\n      </button>\n      <span><img src=\"assets/img/logo.png\"></span>\n    </mat-toolbar>\n    <ng-content></ng-content>\n    <!-- Add Content Here -->\n  </mat-sidenav-content>\n</mat-sidenav-container>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <div *ngFor=\"let article of articles\">\n  <h2>{{article.title}}</h2>\n\n  <p>\n    {{article.description}}\n  </p>\n  <a href=\"{{article.url}}\">Read full article</a>\n</div> -->\n\n<div>\n  <form action=\"\" method=\"\">\n    <label> User Name</label>\n    <input type=\"text\" value=\"username\" >\n  </form>\n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/api.service.ts":
/*!********************************!*\
  !*** ./src/app/api.service.ts ***!
  \********************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let ApiService = class ApiService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.API_KEY = 'bf80fb39094340e4a012c879a0bfa1b3';
    }
    getNews() {
        return this.httpClient.get(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
    }
    AAA_login() {
        var param = {
            "username": "scott",
            "password": "tiger",
            "device_id": "123456",
            "unique_id": "123456"
        };
        const httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        return this.httpClient.post(`https://aaa.allianz.lk/frontend/web/index.php?r=user/getarenamobile`, param, httpOptions);
    }
    save_customer_info() {
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./customer/customer.component */ "./src/app/customer/customer.component.ts");
/* harmony import */ var _news_news_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./news/news.component */ "./src/app/news/news.component.ts");





const routes = [
    { path: 'customer', component: _customer_customer_component__WEBPACK_IMPORTED_MODULE_3__["CustomerComponent"] },
    { path: 'news', component: _news_news_component__WEBPACK_IMPORTED_MODULE_4__["NewsComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'fna';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./main-nav/main-nav.component */ "./src/app/main-nav/main-nav.component.ts");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm2015/toolbar.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm2015/button.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm2015/sidenav.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm2015/icon.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm2015/list.js");
/* harmony import */ var _customer_customer_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./customer/customer.component */ "./src/app/customer/customer.component.ts");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm2015/card.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm2015/drag-drop.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm2015/select.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm2015/stepper.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _news_news_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./news/news.component */ "./src/app/news/news.component.ts");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./api.service */ "./src/app/api.service.ts");
/* harmony import */ var _helper_family_card_family_card_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./helper/family-card/family-card.component */ "./src/app/helper/family-card/family-card.component.ts");
























let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _main_nav_main_nav_component__WEBPACK_IMPORTED_MODULE_6__["MainNavComponent"],
            _customer_customer_component__WEBPACK_IMPORTED_MODULE_13__["CustomerComponent"],
            _news_news_component__WEBPACK_IMPORTED_MODULE_21__["NewsComponent"],
            _helper_family_card_family_card_component__WEBPACK_IMPORTED_MODULE_23__["FamilyCardComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_7__["LayoutModule"],
            _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_8__["MatToolbarModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_9__["MatButtonModule"],
            _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_10__["MatSidenavModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
            _angular_material_list__WEBPACK_IMPORTED_MODULE_12__["MatListModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
            _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_15__["DragDropModule"],
            _angular_material_select__WEBPACK_IMPORTED_MODULE_16__["MatSelectModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
            _angular_material_stepper__WEBPACK_IMPORTED_MODULE_18__["MatStepperModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatButtonToggleModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatCheckboxModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatChipsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatDatepickerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatExpansionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatGridListModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatMenuModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatNativeDateModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatProgressBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatProgressSpinnerModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatRadioModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatRippleModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatSliderModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatSlideToggleModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatSnackBarModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatTableModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatTabsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_19__["MatTooltipModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_20__["HttpClientModule"]
        ],
        providers: [
            _api_service__WEBPACK_IMPORTED_MODULE_22__["ApiService"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/customer/customer.component.scss":
/*!**************************************************!*\
  !*** ./src/app/customer/customer.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".cust-card {\n  height: 500px;\n}\n\n.example-container {\n  width: 400px;\n  max-width: 100%;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top;\n}\n\n.example-list {\n  border: solid 1px #ccc;\n  min-height: 60px;\n  width: 100%;\n  border-radius: 4px;\n  overflow: hidden;\n  display: flex;\n  flex-direction: row;\n}\n\n.example-list1 {\n  height: 340px;\n  max-width: 100%;\n  width: 100%;\n  border: solid 1px #ccc;\n  min-height: 60px;\n  display: flex;\n  flex-direction: row;\n  background: white;\n  border-radius: 4px;\n  overflow: hidden;\n}\n\n.example-box {\n  width: auto;\n  padding: 20px 10px;\n  border-bottom: solid 1px #ccc;\n  color: rgba(0, 0, 0, 0.87);\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n  box-sizing: border-box;\n  cursor: move;\n  background: white;\n  font-size: 14px;\n}\n\n.example-box1 {\n  margin-right: -45px;\n  margin-left: 0px;\n  cursor: move;\n  border-bottom: solid 1px #ccc;\n  box-sizing: border-box;\n  align-self: flex-end;\n}\n\n.cdk-drag-preview {\n  box-sizing: border-box;\n  border-radius: 4px;\n  box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2), 0 8px 10px 1px rgba(0, 0, 0, 0.14), 0 3px 14px 2px rgba(0, 0, 0, 0.12);\n}\n\n.cdk-drag-placeholder {\n  opacity: 0;\n}\n\n.cdk-drag-animating {\n  transition: transform 150ms cubic-bezier(0, 0, 0.2, 1);\n}\n\n/* Animate an item that has been dropped. */\n\n.example-box:last-child {\n  border: none;\n}\n\n.example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\n  transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\n}\n\n.example-container1 {\n  width: 200px;\n  margin: 0 25px 25px 0;\n  display: inline-block;\n  vertical-align: top;\n}\n\n.asd {\n  display: block;\n  align-self: center;\n}\n\n.familycard {\n  max-width: 250px;\n  background-color: #f7f7f7c4;\n  align-self: center;\n  margin-bottom: 8px;\n}\n\n.familycard .row .col-4 {\n  align-self: center;\n}\n\n.col-img {\n  align-content: center;\n}\n\n.familycard img {\n  max-height: 150px;\n  max-width: 50px;\n  align-self: center;\n  align-content: center;\n}\n\n.familycard label {\n  font-size: 8px;\n}\n\n.cardcontent {\n  padding-left: 0px !important;\n}\n\nmat-form-field {\n  width: 100px;\n  font-size: small;\n}\n\n.Income-lable {\n  font-size: small;\n  padding: 0px !important;\n}\n\nmat-slider .salary {\n  width: 100px;\n  padding: 0px !important;\n}\n\nmat-slider .age {\n  padding: 0px !important;\n}\n\nmat-expansion-panel mat-expansion-panel-header mat-panel-title {\n  font-size: small !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbGxpYW56bGlmZWRldi9Eb2N1bWVudHMvRk5BX1Byb2plY3RzL2ZuYV9mZV92MDEvc3JjL2FwcC9jdXN0b21lci9jdXN0b21lci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY3VzdG9tZXIvY3VzdG9tZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FDQ0o7O0FEQ0U7RUFDRSxZQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtBQ0VKOztBRENFO0VBQ0csc0JBQUE7RUFDRCxnQkFBQTtFQUNBLFdBQUE7RUFFQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDQ0o7O0FEQ0U7RUFFRSxhQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0U7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0VKOztBRENFO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQU1BLHNCQUFBO0VBSUEsb0JBQUE7QUNOSjs7QURTRTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxSEFBQTtBQ05KOztBRFdFO0VBQ0UsVUFBQTtBQ1JKOztBRFdFO0VBQ0Usc0RBQUE7QUNSSjs7QURVRSwyQ0FBQTs7QUFHQTtFQUNFLFlBQUE7QUNUSjs7QURZRTtFQUNFLHNEQUFBO0FDVEo7O0FEWUU7RUFDRSxZQUFBO0VBRUEscUJBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDVko7O0FEZUU7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7QUNaSjs7QURlRTtFQUNFLGdCQUFBO0VBQ0EsMkJBQUE7RUFDRCxrQkFBQTtFQUNBLGtCQUFBO0FDWkg7O0FEY0U7RUFDQyxrQkFBQTtBQ1hIOztBRGFBO0VBQ0UscUJBQUE7QUNWRjs7QURZQztFQUNFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QUNUSDs7QURZQztFQUNDLGNBQUE7QUNURjs7QURXQztFQUNFLDRCQUFBO0FDUkg7O0FEVUM7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7QUNQSDs7QURTQTtFQUNHLGdCQUFBO0VBQ0EsdUJBQUE7QUNOSDs7QURTQztFQUNDLFlBQUE7RUFDQSx1QkFBQTtBQ05GOztBRFNBO0VBQ0UsdUJBQUE7QUNORjs7QURRQTtFQUNFLDJCQUFBO0FDTEYiLCJmaWxlIjoic3JjL2FwcC9jdXN0b21lci9jdXN0b21lci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdXN0LWNhcmQge1xuICAgIGhlaWdodDogNTAwcHg7XG4gIH1cbiAgLmV4YW1wbGUtY29udGFpbmVyIHtcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjogMCAyNXB4IDI1cHggMDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgfVxuICBcbiAgLmV4YW1wbGUtbGlzdCB7XG4gICAgIGJvcmRlcjogc29saWQgMXB4ICNjY2M7XG4gICAgbWluLWhlaWdodDogNjBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgIC8vIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgfVxuICAuZXhhbXBsZS1saXN0MSB7XG4gICAgLy8gd2lkdGg6IDEwMDBweDtcbiAgICBoZWlnaHQ6IDM0MHB4O1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xuICAgIG1pbi1oZWlnaHQ6IDYwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICB9XG4gIC5leGFtcGxlLWJveCB7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgcGFkZGluZzogMjBweCAxMHB4O1xuICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjY2NjO1xuICAgIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuODcpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgY3Vyc29yOiBtb3ZlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuXG4gIC5leGFtcGxlLWJveDEge1xuICAgIG1hcmdpbi1yaWdodDogLTQ1cHg7XG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgICBjdXJzb3I6IG1vdmU7XG4gICAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNjY2M7XG4gICAgLy9jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg3KTtcbiAgICAvLyBkaXNwbGF5OiBmbGV4O1xuICAgIC8vIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgLy8gYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAvL2p1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIC8vIGN1cnNvcjogbW92ZTtcbiAgICAvLyBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAvLyBmb250LXNpemU6IDE0cHg7XG4gICAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG4gIH1cbiAgXG4gIC5jZGstZHJhZy1wcmV2aWV3IHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3gtc2hhZG93OiAwIDVweCA1cHggLTNweCByZ2JhKDAsIDAsIDAsIDAuMiksXG4gICAgICAgICAgICAgICAgMCA4cHggMTBweCAxcHggcmdiYSgwLCAwLCAwLCAwLjE0KSxcbiAgICAgICAgICAgICAgICAwIDNweCAxNHB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICB9XG4gIFxuICAuY2RrLWRyYWctcGxhY2Vob2xkZXIge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgXG4gIC5jZGstZHJhZy1hbmltYXRpbmcge1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAxNTBtcyBjdWJpYy1iZXppZXIoMCwgMCwgMC4yLCAxKTtcbiAgfVxuICAvKiBBbmltYXRlIGFuIGl0ZW0gdGhhdCBoYXMgYmVlbiBkcm9wcGVkLiAqL1xuXG4gIFxuICAuZXhhbXBsZS1ib3g6bGFzdC1jaGlsZCB7XG4gICAgYm9yZGVyOiBub25lO1xuICB9XG4gIFxuICAuZXhhbXBsZS1saXN0LmNkay1kcm9wLWxpc3QtZHJhZ2dpbmcgLmV4YW1wbGUtYm94Om5vdCguY2RrLWRyYWctcGxhY2Vob2xkZXIpIHtcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XG4gIH0gIFxuICBcbiAgLmV4YW1wbGUtY29udGFpbmVyMSB7XG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIC8vbWF4LXdpZHRoOiA1MCU7XG4gICAgbWFyZ2luOiAwIDI1cHggMjVweCAwO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xuICB9XG5cblxuICAvLyBEYW1pdGggPT09PSsrKysrK1xuICAuYXNke1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGFsaWduLXNlbGY6IGNlbnRlciA7IFxuICB9XG5cbiAgLmZhbWlseWNhcmR7XG4gICAgbWF4LXdpZHRoOiAyNTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3YzQ7XG4gICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gIH1cbiAgLmZhbWlseWNhcmQgLnJvdyAuY29sLTR7XG4gICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gIH1cbi5jb2wtaW1ne1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG59XG4gLmZhbWlseWNhcmQgaW1ne1xuICAgbWF4LWhlaWdodDogMTUwcHg7XG4gICBtYXgtd2lkdGg6IDUwcHg7XG4gICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICBcbiB9XG4gLmZhbWlseWNhcmQgbGFiZWx7XG4gIGZvbnQtc2l6ZTogOHB4O1xuIH1cbiAuY2FyZGNvbnRlbnR7XG4gICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xuIH1cbiBtYXQtZm9ybS1maWVsZHtcbiAgIHdpZHRoOiAxMDBweDtcbiAgIGZvbnQtc2l6ZTogc21hbGw7XG4gfVxuLkluY29tZS1sYWJsZXtcbiAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbiB9XG5cbiBtYXQtc2xpZGVyIC5zYWxhcnkge1xuICB3aWR0aDogMTAwcHg7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5tYXQtc2xpZGVyIC5hZ2Uge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cbm1hdC1leHBhbnNpb24tcGFuZWwgbWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXIgbWF0LXBhbmVsLXRpdGxle1xuICBmb250LXNpemU6IHNtYWxsICFpbXBvcnRhbnQ7XG59XG5cbiIsIi5jdXN0LWNhcmQge1xuICBoZWlnaHQ6IDUwMHB4O1xufVxuXG4uZXhhbXBsZS1jb250YWluZXIge1xuICB3aWR0aDogNDAwcHg7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgbWFyZ2luOiAwIDI1cHggMjVweCAwO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHZlcnRpY2FsLWFsaWduOiB0b3A7XG59XG5cbi5leGFtcGxlLWxpc3Qge1xuICBib3JkZXI6IHNvbGlkIDFweCAjY2NjO1xuICBtaW4taGVpZ2h0OiA2MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xufVxuXG4uZXhhbXBsZS1saXN0MSB7XG4gIGhlaWdodDogMzQwcHg7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogc29saWQgMXB4ICNjY2M7XG4gIG1pbi1oZWlnaHQ6IDYwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5leGFtcGxlLWJveCB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nOiAyMHB4IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjY2NjO1xuICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjg3KTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGN1cnNvcjogbW92ZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmV4YW1wbGUtYm94MSB7XG4gIG1hcmdpbi1yaWdodDogLTQ1cHg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIGN1cnNvcjogbW92ZTtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4ICNjY2M7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xufVxuXG4uY2RrLWRyYWctcHJldmlldyB7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgYm94LXNoYWRvdzogMCA1cHggNXB4IC0zcHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDhweCAxMHB4IDFweCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDNweCAxNHB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuXG4uY2RrLWRyYWctcGxhY2Vob2xkZXIge1xuICBvcGFjaXR5OiAwO1xufVxuXG4uY2RrLWRyYWctYW5pbWF0aW5nIHtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDE1MG1zIGN1YmljLWJlemllcigwLCAwLCAwLjIsIDEpO1xufVxuXG4vKiBBbmltYXRlIGFuIGl0ZW0gdGhhdCBoYXMgYmVlbiBkcm9wcGVkLiAqL1xuLmV4YW1wbGUtYm94Omxhc3QtY2hpbGQge1xuICBib3JkZXI6IG5vbmU7XG59XG5cbi5leGFtcGxlLWxpc3QuY2RrLWRyb3AtbGlzdC1kcmFnZ2luZyAuZXhhbXBsZS1ib3g6bm90KC5jZGstZHJhZy1wbGFjZWhvbGRlcikge1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMjUwbXMgY3ViaWMtYmV6aWVyKDAsIDAsIDAuMiwgMSk7XG59XG5cbi5leGFtcGxlLWNvbnRhaW5lcjEge1xuICB3aWR0aDogMjAwcHg7XG4gIG1hcmdpbjogMCAyNXB4IDI1cHggMDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xufVxuXG4uYXNkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLmZhbWlseWNhcmQge1xuICBtYXgtd2lkdGg6IDI1MHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjdmN2Y3YzQ7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xufVxuXG4uZmFtaWx5Y2FyZCAucm93IC5jb2wtNCB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLmNvbC1pbWcge1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5mYW1pbHljYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDE1MHB4O1xuICBtYXgtd2lkdGg6IDUwcHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xufVxuXG4uZmFtaWx5Y2FyZCBsYWJlbCB7XG4gIGZvbnQtc2l6ZTogOHB4O1xufVxuXG4uY2FyZGNvbnRlbnQge1xuICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xufVxuXG5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDBweDtcbiAgZm9udC1zaXplOiBzbWFsbDtcbn1cblxuLkluY29tZS1sYWJsZSB7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5tYXQtc2xpZGVyIC5zYWxhcnkge1xuICB3aWR0aDogMTAwcHg7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5tYXQtc2xpZGVyIC5hZ2Uge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxubWF0LWV4cGFuc2lvbi1wYW5lbCBtYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlciBtYXQtcGFuZWwtdGl0bGUge1xuICBmb250LXNpemU6IHNtYWxsICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/customer/customer.component.ts":
/*!************************************************!*\
  !*** ./src/app/customer/customer.component.ts ***!
  \************************************************/
/*! exports provided: CustomerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerComponent", function() { return CustomerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm2015/drag-drop.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");




let CustomerComponent = class CustomerComponent {
    // tslint:disable-next-line:variable-name
    constructor(_formBuilder) {
        this._formBuilder = _formBuilder;
        this.labels = ['50000', '75000', '100000', '200000'];
        this.isLinear = false;
        this.amount_father = "0";
        this.amount_mother = "0";
        this.father = false;
        this.mother = false;
        this.boy01 = false;
        this.girl01 = false;
        this.panelOpenState = false;
        this.family = [
            {
                title: 'Father',
                image: 'assets/img/main.png'
            },
            {
                title: 'Mother',
                image: 'assets/img/wife.png'
            },
            {
                title: 'Boy 01',
                image: 'assets/img/child_m.png'
            },
            {
                title: 'Girl 01',
                image: 'assets/img/child_f.png'
            },
            {
                title: 'Boy 02',
                image: 'assets/img/child_m.png'
            },
            {
                title: 'Girl 02',
                image: 'assets/img/child_f.png'
            },
            {
                title: 'Boy 03',
                image: 'assets/img/child_m.png'
            },
            {
                title: 'Girl 03',
                image: 'assets/img/child_f.png'
            }
        ];
        //080610 token number
        this.done = [];
        this.ocupation = [
            { value: 'SE', viewValue: 'Software Engineer' },
            { value: 'ME', viewValue: 'Mechanical Engineer' },
            { value: 'EE', viewValue: 'Engineer' }
        ];
    }
    ngOnInit() {
        // this.done.push({
        //   title: 'Mother',
        //   image: 'assets/img/wife.png'
        // });
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
        this.thirdFormGroup = this._formBuilder.group({
            thirdCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    }
    drop(event) {
        if (event.previousContainer === event.container) {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__["moveItemInArray"])(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            Object(_angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_2__["transferArrayItem"])(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            console.log(event.container.data.length);
            console.log(event.container.id);
            console.log(JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title);
            if (event.container.id == "cdk-drop-list-0") {
                var Selecttype = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title;
                if (Selecttype === "Father") {
                    this.father = true;
                }
                if (Selecttype === "Mother") {
                    this.mother = true;
                }
                if (Selecttype === "Boy 01") {
                    this.boy01 = true;
                }
                if (Selecttype === "Girl 01") {
                    this.girl01 = true;
                }
            }
            else if (event.container.id == "cdk-drop-list-1") {
                var Selecttype = JSON.parse(JSON.stringify(event.container.data[event.currentIndex])).title;
                if (Selecttype === "Father") {
                    this.father = false;
                }
                if (Selecttype === "Mother") {
                    this.mother = false;
                }
                if (Selecttype === "Boy 01") {
                    this.boy01 = false;
                }
                if (Selecttype === "Girl 01") {
                    this.girl01 = false;
                }
            }
        }
    }
    formatLabel(value) {
        return Math.round(value * 10000);
    }
    SalaryLabel(value) {
        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }
        return value;
    }
    changeIncome(event, type) {
        if (type == "F") {
            this.amount_father = Math.round(event.value / 1000) + 'k';
        }
        if (type == "M") {
            this.amount_mother = Math.round(event.value / 1000) + 'k';
        }
    }
    changeAge(event, type) {
        if (type == "F") {
            this.age_father = event.value;
        }
        if (type == "M") {
            this.age_mother = event.value;
        }
        if (type == "B1") {
            this.age_boy01 = event.value;
        }
        if (type == "G1") {
            this.age_girl01 = event.value;
        }
    }
    changeHotel(event) {
        this.hotel_expenses = Math.round(event.value / 1000) + 'k';
    }
    changeCritical_Illness(event) {
        this.Critical_Illness = Math.round(event.value / 1000) + 'k';
    }
    changeDisability_expenses(event) {
        this.Disability_expenses = Math.round(event.value / 1000) + 'k';
    }
    changeRetirement_fund(event) {
        this.Retirement_fund = Math.round(event.value / 1000) + 'k';
    }
    changeLife_Insurance(event) {
        this.Life_Insurance = Math.round(event.value / 1000) + 'k';
    }
};
CustomerComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] }
];
CustomerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-customer',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./customer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/customer/customer.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./customer.component.scss */ "./src/app/customer/customer.component.scss")).default]
    })
], CustomerComponent);



/***/ }),

/***/ "./src/app/helper/family-card/family-card.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/helper/family-card/family-card.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlbHBlci9mYW1pbHktY2FyZC9mYW1pbHktY2FyZC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/helper/family-card/family-card.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/helper/family-card/family-card.component.ts ***!
  \*************************************************************/
/*! exports provided: FamilyCardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FamilyCardComponent", function() { return FamilyCardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FamilyCardComponent = class FamilyCardComponent {
    constructor() { }
    ngOnInit() {
    }
};
FamilyCardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-family-card',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./family-card.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/helper/family-card/family-card.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./family-card.component.scss */ "./src/app/helper/family-card/family-card.component.scss")).default]
    })
], FamilyCardComponent);



/***/ }),

/***/ "./src/app/main-nav/main-nav.component.scss":
/*!**************************************************!*\
  !*** ./src/app/main-nav/main-nav.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbGxpYW56bGlmZWRldi9Eb2N1bWVudHMvRk5BX1Byb2plY3RzL2ZuYV9mZV92MDEvc3JjL2FwcC9tYWluLW5hdi9tYWluLW5hdi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbWFpbi1uYXYvbWFpbi1uYXYuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxtQkFBQTtBQ0NGOztBREVBO0VBQ0Usd0JBQUE7RUFBQSxnQkFBQTtFQUNBLE1BQUE7RUFDQSxVQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9tYWluLW5hdi9tYWluLW5hdi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zaWRlbmF2LWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnNpZGVuYXYge1xuICB3aWR0aDogMjAwcHg7XG59XG5cbi5zaWRlbmF2IC5tYXQtdG9vbGJhciB7XG4gIGJhY2tncm91bmQ6IGluaGVyaXQ7XG59XG5cbi5tYXQtdG9vbGJhci5tYXQtcHJpbWFyeSB7XG4gIHBvc2l0aW9uOiBzdGlja3k7XG4gIHRvcDogMDtcbiAgei1pbmRleDogMTtcbn1cbiIsIi5zaWRlbmF2LWNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnNpZGVuYXYge1xuICB3aWR0aDogMjAwcHg7XG59XG5cbi5zaWRlbmF2IC5tYXQtdG9vbGJhciB7XG4gIGJhY2tncm91bmQ6IGluaGVyaXQ7XG59XG5cbi5tYXQtdG9vbGJhci5tYXQtcHJpbWFyeSB7XG4gIHBvc2l0aW9uOiBzdGlja3k7XG4gIHRvcDogMDtcbiAgei1pbmRleDogMTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/main-nav/main-nav.component.ts":
/*!************************************************!*\
  !*** ./src/app/main-nav/main-nav.component.ts ***!
  \************************************************/
/*! exports provided: MainNavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainNavComponent", function() { return MainNavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm2015/layout.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");




let MainNavComponent = class MainNavComponent {
    constructor(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(result => result.matches), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["shareReplay"])());
    }
};
MainNavComponent.ctorParameters = () => [
    { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"] }
];
MainNavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'main-nav',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-nav.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/main-nav/main-nav.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-nav.component.scss */ "./src/app/main-nav/main-nav.component.scss")).default]
    })
], MainNavComponent);



/***/ }),

/***/ "./src/app/news/news.component.scss":
/*!******************************************!*\
  !*** ./src/app/news/news.component.scss ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ld3MvbmV3cy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/news/news.component.ts":
/*!****************************************!*\
  !*** ./src/app/news/news.component.ts ***!
  \****************************************/
/*! exports provided: NewsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsComponent", function() { return NewsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api.service */ "./src/app/api.service.ts");



let NewsComponent = class NewsComponent {
    constructor(apiService) {
        this.apiService = apiService;
    }
    ngOnInit() {
        // this.apiService.getNews().subscribe((data)=>{
        //  // console.log(data);
        //   this.articles = data['articles'];
        // });
        this.aaa_login_post();
    }
    aaa_login_post() {
        this.apiService.AAA_login().subscribe(data => {
            console.log(data);
        });
    }
};
NewsComponent.ctorParameters = () => [
    { type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"] }
];
NewsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-news',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./news.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./news.component.scss */ "./src/app/news/news.component.scss")).default]
    })
], NewsComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");






if (_environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_4__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/allianzlifedev/Documents/FNA_Projects/fna_fe_v01/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);